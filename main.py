import pytest
import os
import shutil
from common.logger import Logger
from until.clear_results import clear_allure

if __name__ == '__main__':
    Logger.info(r"""
                                                              _ooOoo_
                                                             o8888888o
                                                             88" . "88
                                                             (| -_- |)
                                                              O\ = /O
                                                          ____/`---'\____
                                                        .   ' \\| |// `.
                                                         / \\||| : |||// \
                                                       / _||||| -:- |||||- \
                                                         | | \\\ - /// | |
                                                       | \_| ''\---/'' | |
                                                        \ .-\__ `-` ___/-. /
                                                     ___`. .' /--.--\ `. . __
                                                  ."" '< `.___\_<|>_/___.' >'"".
                                                 | | : `- \`.;`\ _ /`;.`/ - ` : | |
                                                   \ \ `-. \_ __\ /__ _/ .-` / /
                                           ======`-.____`-.___\_____/___.-`____.-'======
                                                              `=---='

                                           .............................................
                                                  佛祖保佑             永无BUG
                                           .............................................
                                               佛曰:
                                               写字楼里写字间          写字间里程序员；
                                               程序人员写程序          又拿程序换酒钱。
                                               酒醒只在网上坐          酒醉还来网下眠； 
                                               酒醉酒醒日复日          网上网下年复年。
                                               但愿老死电脑间          不愿鞠躬老板前；
                                               奔驰宝马贵者趣          公交自行程序员。
                                               别人笑我忒疯癫          我笑自己命太贱；
                                               不见满街漂亮妹          哪个归得程序员？
    """)

    """解决allure报告缓存和Jenkins无文件报错"""
    clear_allure()

    # pytest.main(['-m','smoke'])
    pytest.main()
    os.system(r"allure generate ./allure-results -o ./allure-report --clean")

    os.system('allure open allure-report')  # 打开报告

#失败重试
# • 测试失败后要重新运行n次，要在重新运行之间添加延迟时 间，间隔n秒再运行1。
# • 执行:
# • 安装:pip install pytest-rerunfailures
# • pytest -v - -reruns 5 --reruns-delay 1 —每次等1秒 重试5次



